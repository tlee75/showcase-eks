provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.11"
}

resource "kubernetes_deployment" "showcase-api" {
  metadata {
    name = "showcase-api"
    labels = {
      test = "ShowcaseAPI"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "ShowcaseAPI"
      }
    }

    template {
      metadata {
        labels = {
          test = "ShowcaseAPI"
        }
      }

      spec {
        container {
          image = "378255292555.dkr.ecr.us-east-1.amazonaws.com/showcase-api:latest"
          name  = "showcase-api"

          resources {
            limits {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "showcase-api" {
  metadata {
    name = "showcase-api"
  }
  spec {
    selector = {
      test = "ShowcaseAPI"
    }
    port {
      port        = 80
      target_port = 5000
    }

    type = "LoadBalancer"
  }
}