terraform {
  required_version = ">= 0.12.0"
  backend "s3" {
    encrypt = true
  }
}
