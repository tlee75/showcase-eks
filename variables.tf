variable "region" {}
variable "aws_profile" {}
variable "cluster_name" {}
variable "domain_name" {}
variable "env_tag" {}
variable "inventory_code_tag" {}
variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = [
    "378255292555"
  ]
}
variable map_roles {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::378255292555:user/Administrator"
      username = "Administrator"
      groups   = ["system:masters"]
    }
  ]
}
variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::378255292555:user/Administrator"
      username = "Administrator"
      groups   = ["system:masters"]
    }
  ]
}